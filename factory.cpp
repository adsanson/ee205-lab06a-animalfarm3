///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file factory.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   21_03_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstdlib>
#include <random>

#include "animal.hpp"
#include "dog.hpp"
#include "cat.hpp"
#include "nunu.hpp"
#include "nene.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "factory.hpp"

using namespace std;

namespace animalfarm {

static random_device rd;
static mt19937_64 rng(rd());
static bernoulli_distribution boolrng(0.5);

Animal* AnimalFactory::getRandomAnimal(){
   Animal* newAnimal = NULL;
   uniform_int_distribution<> animalrng(0,5);
   int i = animalrng(rng);
      switch (i) {
         case 0: 
            newAnimal = new Cat( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
         case 1: 
            newAnimal = new Dog( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
         case 2: 
            newAnimal = new Nunu( Animal::getRandomBool(), RED, Animal::getRandomGender()); break;
         case 3: 
            newAnimal = new Aku( Animal::getRandomWeight(18.0, 24.0), SILVER, Animal::getRandomGender()); break; 
         case 4: 
            newAnimal = new Palila( Animal::getRandomName(), YELLOW, Animal::getRandomGender()); break;
         case 5: 
            newAnimal = new Nene( Animal::getRandomName(), BROWN, Animal::getRandomGender()); break;
      }
      return newAnimal;

   }

}
