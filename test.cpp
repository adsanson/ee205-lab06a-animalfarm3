///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file test.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   21_03_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "bird.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "fish.hpp"
#include "factory.hpp"

using namespace std;
using namespace animalfarm;

int main(){

   cout << "Gender " << Animal::getRandomGender() << endl;
   cout << "Color " << Animal::getRandomColor() << endl;
   cout << "Bool  " << boolalpha << Animal::getRandomBool() << endl;
   cout << "Weight   " << Animal::getRandomWeight(18.0, 22.0) << endl;
   cout << "Name  " << Animal::getRandomName() << endl;

   for (int i = 0; i < 25; i++ ) {
      Animal* a = AnimalFactory::getRandomAnimal();
      cout << a->speak() << endl;
   }
}
